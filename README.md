# Git cheatsheet

## Typical workflow
 - `git init` - initialize your repository
 - `git add <filename>` - stage file (`git add *` to add all files)
 - `git commit -m 'PUT YOUR COMMIT MESSAGE HERE'` - Commit the changes you made

## Pushing your changes to Gitlab
 -  `git remote add origin <name of your remote repo link>`

 - `git push -u origin master` - push to the remote repo


## Pulling stuff from Gitlab
- `git pull origin master` - pull from an existing remote repository

- `git clone <url to remote repo>` - clones a remote repo <someone elses repo>

## Other useful commands
 -  `git status` - tells you the current state of the repo
 -  `git log` - see the previous commit message